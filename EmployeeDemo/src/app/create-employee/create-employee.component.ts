import { Component, OnInit } from '@angular/core';
import { Department } from '../models/department.model';
import { FormGroup,  FormBuilder, Validators, FormControl} from '@angular/forms';
import { Employee } from '../models/employee.model';

@Component({
  selector: 'app-create-employee',
  templateUrl: './create-employee.component.html',
  styleUrls: ['./create-employee.component.css']
})
export class CreateEmployeeComponent implements OnInit {
  
    employeeForm: FormGroup;
    

      previewPhoto= false;
      departments:Department[]=[
        {id:1, name:"Help Desk"},
        {id:2, name:"HR"},
        {id:3, name:"IT"},
        {id:4, name:"payroll"}
      ];

      validationMessage={
        'fullName':{
          'required':'Full Name is required',
          'minlength':'Full Name must be greater than 2 characters',  
          'maxlength':'Full Name must be less than 10 characters'   
        },
        'email':{
          'required':'email is required'
        },
        'skillName':{
          'required':'skill Name is required',
        },
        'experienceInYears':{
          'required':'Experience is required',
        },
        'proficiency':{
          'required':'proficiency is required',
        },
      };

      formErrors={
        'fullName':'',
        'email':'',
        'skillName':'',
        'experienceInYears':'',
        'proficiency':''

      };

  constructor() { }

  

  ngOnInit(): void {
    this.employeeForm=new FormGroup({
      fullName: new FormControl('', [Validators.required, Validators.minLength(2), Validators.maxLength(10)]),
      email:new FormControl ('',Validators.required),
      skills:new FormGroup({
        skillName:new FormControl('',Validators.required),
        experienceInYears : new FormControl('',Validators.required),
        proficiency:new FormControl('',Validators.required)
      }),
    });
    this.employeeForm.valueChanges.subscribe(data=>
      {
        this.LogValidationError(this.employeeForm);
      });
  }
  
   LogValidationError(group: FormGroup=this.employeeForm):void{
     Object.keys(group.controls).forEach((key:string) =>{
       const abstarctControl=group.get(key);
       if(abstarctControl instanceof FormGroup){
         this.LogValidationError(abstarctControl);
       }else{
         this.formErrors[key]='';
         if(!abstarctControl.valid){
           const message=this.validationMessage[key];
           
           for(const errorKey in abstarctControl.errors){
             if(errorKey){
               this.formErrors[key]+=message[errorKey] +'';
             }
           }
         }
       }

     });

   }

   onLoadData()
   {
     this.LogValidationError(this.employeeForm);
   }

  onSubmit(): void{
    console.log(this.employeeForm.value);
  }
  

}
