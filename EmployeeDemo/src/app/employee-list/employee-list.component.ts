import { Component, OnInit } from '@angular/core';
import { Employee } from '../models/employee.model';

@Component({
  selector:'app-create-employee',
  templateUrl: './employee-list.component.html',
  styleUrls: ['./employee-list.component.css']
})
export class EmployeeListComponent implements OnInit {
  employees: Employee[]= [
    {
      id:1,
      fullName: 'Mark',
      gender: 'Male',
      contactPreference:'Email',
      email:'abc@gmai.com',
      dateOfBirth: new Date('01/01/1990'),
      department: 'IT',
      inActive: true,
      photopath: 'assets/images/employeeimg.png'


    },
    {
    id:2,
    fullName: 'Mark1',
    gender: 'Male',
    contactPreference:'Phone',
    phoneNumber: 9087654321,
    dateOfBirth: new Date('01/01/1990'),
    department: 'IT',
    inActive: true,
    photopath: 'assets/images/employeeimage.jfif'
    },
    {
      id:3,
      fullName: 'Mark2',
      gender: 'Male',
      contactPreference:'Email',
      dateOfBirth: new Date('01/01/1990'),
      department: 'IT',
      inActive: true,
      photopath: 'assets/images/employeeimg.png'
    }
    
  ];

  constructor() { }

  ngOnInit(): void {
  }

}
