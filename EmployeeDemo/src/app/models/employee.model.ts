export class Employee{
    id: number;
    fullName: string;
    gender: string;
    email?: string;
    phoneNumber?: number;
    contactPreference: string;
    dateOfBirth: Date;
    department: string;
    inActive: boolean;
    photopath?: string;

}